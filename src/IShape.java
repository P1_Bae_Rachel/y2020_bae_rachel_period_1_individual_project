import javafx.scene.paint.Color;

public class IShape extends Shape{
	
	public IShape(int rows, int cols) {
		super(rows, cols,Color.BLUE);
		positions = new int[4][2];
		positions[0][0] = 0;
		positions[0][1] = getNumCols()/2;
		positions[1][0] = 0;
		positions[1][1] = getNumCols()/2-1;
		positions[2][0] = 0;
		positions[2][1] = getNumCols()/2+1;
		positions[3][0] = 0;
		positions[3][1] = getNumCols()/2-2;
	}

	@Override
	public void rotate() {
		super.rotate();
		if(getRowOf(3) == getRowOf(0) && getColOf(3) == getColOf(0)+2) {
			setPosOf(3,getRowOf(0)+2, getColOf(0));
		} else if(getRowOf(3) == getRowOf(0) && getColOf(3) == getColOf(0)-2) {
			setPosOf(3,getRowOf(0)-2, getColOf(0));
		} else if(getRowOf(3) == getRowOf(0) +2 && getColOf(3) == getColOf(0)) {
			setPosOf(3,getRowOf(0), getColOf(0)-2);
		} else if(getRowOf(3) == getRowOf(0) - 2 && getColOf(3) == getColOf(0)) {
			setPosOf(3,getRowOf(0), getColOf(0)+2);
		}
	}
	

	

}
