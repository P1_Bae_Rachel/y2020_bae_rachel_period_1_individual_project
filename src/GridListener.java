import javafx.scene.paint.Color;

public interface GridListener {
	
	public void cellChanged(int row, int col, Color newColor);
//	public void gridReplaced();
}
