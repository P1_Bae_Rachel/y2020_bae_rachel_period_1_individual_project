import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class View extends Group implements GridListener{
	private double tileSize;

	private Rectangle[][] cells;
	
	private Model model;
	
	public View() {
		this.model = null;
		this.cells = null;
		this.tileSize = 30;
	}
	
	public void setTileSize(double size) {
		this.tileSize = size;
	}
	

	public double getTileSize() {
		return this.tileSize;
	}
	
	public void setModel(Model model) {
		if (this.model != null) {
			this.model.removeGridListener(this);
		}
		this.model = model;
		model.addGridListener(this);
		resetCells();
	}
	
	public Rectangle cellAtGridCoords(int row, int col) {
		return cells[row][col];
	}
	
	public double xPosForCol(int col) {
		return col * tileSize;
	}
	
	public double yPosForRow(int row) {
		return row * tileSize;
	}
	

	public int colForXPos(double x) {
		return (int)(x / tileSize);
	}
	
	public int rowForYPos(double y) {
		return (int)(y / tileSize);
	}
	
	public void resetCells() {
		getChildren().remove(0, getChildren().size());
		if (model != null) {
			cells = new Rectangle[model.getNumRows()][model.getNumCols()];
			for (int row = 0; row < model.getNumRows(); row++) {
				for (int col = 0; col < model.getNumCols(); col++) {
					Rectangle cell = new Rectangle(tileSize, tileSize);
//					cell.setFill(model.getColorAt(row, col));
					cell.setFill(Color.WHITE);
					cell.setX(tileSize * col);
					cell.setY(tileSize * row);
					cell.setStroke(Color.BLACK);
					cell.setStrokeWidth(1);
					getChildren().add(cell);
					cells[row][col] = cell;
				}
			}
		}
	}
	
	public void gridReplaced() {
		resetCells();
	}

	public void cellChanged(int row, int col, Color newColor) {
		cells[row][col].setFill(newColor);
		
	}

}
