import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Controller extends Application implements LevelListener{
	
	MyAnimationTimer timer;
	Model model;
	View view;
	Stage mainStage;
	HBox information;
	Button startButton;
	Button viewButton;
	Scene titleScene;
	Scene gameScene;
	Scene gameOverScene;
	BorderPane gameRoot;
	Scene instructionScene;
	Button backButton;
	Button pauseButton;
	Button redoButton;
	Label levelLabel;
	Label lineLabel;

	
	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage stage) throws Exception {
		timer = new MyAnimationTimer();
		timer.setDelay(0.8);
		
		mainStage = stage;
		
		mainStage.setTitle("TETRIS");
		MouseEventHandler meh = new MouseEventHandler();
		KeyEventHandler keh = new KeyEventHandler();
		
		//Game
		gameRoot = new BorderPane();
		model = new Model();
		model.addLevelListener(this);
		view = new View();
		view.setModel(model);
		view.setTileSize(30);
		gameRoot = new BorderPane();
		gameRoot.setCenter(view);
		gameRoot.setOnKeyPressed(keh);
		gameScene = new Scene(gameRoot);
		information = new HBox();
		levelLabel = new Label("Level: " + model.getLevel());
		lineLabel = new Label("Lines Cleared: " + model.getNumLinesCleared());
		pauseButton = new Button("Pause Game");
		pauseButton.setOnMouseClicked(meh);
		information.getChildren().addAll(levelLabel, lineLabel, pauseButton);
		gameRoot.setBottom(information);
		gameRoot.setPadding(new Insets(10));
		information.setPadding(new Insets(10));
		information.setSpacing(10);
		information.setAlignment(Pos.TOP_CENTER);
		
		
		//Title
		VBox titleRoot = new VBox();
		Label title = new Label("TETRIS");
		title.setFont(new Font(30));
		startButton = new Button("Start Level " + model.getLevel());
		viewButton = new Button("View Game Instructions");
		startButton.setOnMouseClicked(meh);
		viewButton.setOnMouseClicked(meh);
		titleRoot.getChildren().addAll(title, startButton, viewButton);
		titleRoot.setSpacing(10);
		titleRoot.setPadding(new Insets(10));
		titleRoot.setAlignment(Pos.CENTER);
		
		titleScene = new Scene(titleRoot);
		
		//Instructions
		VBox instructions = new VBox();
		Label instructionLabel = new Label("Instructions!");
		instructionLabel.setFont(new Font(30));
		Text text = new Text("How to win: \nThis game follows the basic Tetris rules, but the point system is different. "
				+ "\nFor each level, you must fill 5 lines (I made it short for demo purposes). "
				+ "\nIf you would like to limit the shapes to the I shape and O shape, go to the Model class,"
				+ "\nNavigate to update(), which is the last method, and change the random integer value."
				+ "\nIf you succeed, you will go to the next level. "
				+ "\nThe right, left, and down arrows are for directions of the shapes."
				+ "\nThe up arrow will rotate the shape.");
		text.setFont(new Font(20));
		backButton = new Button("Back to title screen");
		instructions.getChildren().addAll(instructionLabel, text, backButton);
		instructions.setAlignment(Pos.CENTER);
		instructions.setSpacing(20);
		instructions.setPadding(new Insets(20));
		backButton.setOnMouseClicked(meh);
		instructionScene = new Scene(instructions);
		
		//Game Over Scene
		VBox overRoot = new VBox();
		Label overLabel = new Label("GAME OVER");
		overLabel.setFont(new Font(30));
		redoButton = new Button("Start Again");
		redoButton.setOnMouseClicked(meh);
		overRoot.getChildren().addAll(overLabel, redoButton);
		overRoot.setSpacing(10);
		overRoot.setPadding(new Insets(10));
		overRoot.setAlignment(Pos.CENTER);
		gameOverScene = new Scene(overRoot);
		
		stage.setScene(titleScene);
		stage.sizeToScene();
		stage.show();
		
	}
	
	private class KeyEventHandler implements EventHandler<KeyEvent>{

		@Override
		public void handle(KeyEvent event) {
			              
			if(event.getCode().equals(KeyCode.LEFT) || event.getCode().equals(KeyCode.RIGHT) || event.getCode().equals(KeyCode.DOWN)) {
				model.move(event.getCode());
			} else if(event.getCode().equals(KeyCode.UP)) {
				model.rotate();
			}
		}
		
	}
	
	private class MouseEventHandler implements EventHandler<MouseEvent>{
		@Override
		public void handle(MouseEvent event) {
			if(event.getSource() == startButton) { 
				levelLabel.setText("Level: " + model.getLevel());
				mainStage.setScene(gameScene);
				mainStage.sizeToScene();
				timer.start();
				gameRoot.requestFocus();
			} else if(event.getSource() == viewButton) { 
				mainStage.setScene(instructionScene);
			} else if (event.getSource() == backButton) { 
				mainStage.setScene(titleScene);
			} else if (event.getSource() == pauseButton) {
				timer.stop();
				startButton.setText("Continue Level " + model.getLevel());
				mainStage.setScene(titleScene);
			} else if (event.getSource() == redoButton) {
				timer.setDelay(0.8);
				startButton.setText("Level: " + model.getLevel());
				model.clearModel();
				view.resetCells();
				model.setNumLinesCleared(0);
				model.setLevel(0);
				model.setNumLinesCleared(0);
				mainStage.setScene(titleScene);
			}
			
		}
		
	}
	
	private class MyAnimationTimer extends AnimationTimer{
		
		long previousTime;
		double delay = 0.7;
		
		@Override
		public void handle(long now) {
			if(now - previousTime > getDelay() * 1e9) {
				model.update();
				previousTime = now;
			}
		}
		
		public double getDelay() {
			return delay;
		}
		
		public void setDelay(double d) {
			delay = d;
		}
		
	}


	@Override
	public void levelChanged(int oldVal, int newVal) {
		timer.stop();
		model.clearModel();
		view.resetCells();
		model.setNumLinesCleared(0);
		startButton.setText("Start Level " + model.getLevel());
		mainStage.setScene(titleScene);
		mainStage.show();
		timer.setDelay(timer.getDelay()-0.05);
		
	}

	@Override
	public void numLinesCleared(int numCleared) {
		lineLabel.setText("Lines Cleared: " + numCleared);
		
	}

	@Override
	public void gameOver() {
		timer.stop();
		model.clearModel();
		view.resetCells();
		model.setNumLinesCleared(0);
		model.setLevel(0);
		model.setNumLinesCleared(0);
		startButton.setText("Start Again");
		mainStage.setScene(gameOverScene);
		mainStage.show();
		
	}

}