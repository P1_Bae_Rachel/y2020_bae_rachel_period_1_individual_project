import javafx.scene.paint.Color;

public class Oshape extends Shape{

	public Oshape(int rows, int cols) {
		super(rows, cols,Color.RED);
		positions = new int[4][2];
		positions[0][0] = 0;
		positions[0][1] = getNumCols()/2;
		positions[1][0] = 0;
		positions[1][1] = getNumCols()/2-1;
		positions[2][0] = 1;
		positions[2][1] = getNumCols()/2;
		positions[3][0] = 1;
		positions[3][1] = getNumCols()/2-1;
	}

	@Override
	public void rotate() {
		
	}

}
