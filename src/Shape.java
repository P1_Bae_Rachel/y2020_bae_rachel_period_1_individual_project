import javafx.scene.paint.Color;

public class Shape {

	int[][] positions;
	private Color color;
	private boolean isFixed;
	private int rowLimit, colLimit;

	public Shape(int rows, int cols, Color c) {
		rowLimit = rows;
		colLimit = cols;
		color = c;
		isFixed = false;
	}

	
	public int getNumRows() {
		return rowLimit;
	}
	
	public int getNumCols() {
		return colLimit;
	}
	
	public int getRowOf(int num) {
		return positions[num][0];
	}
	public int getColOf(int num) {
		return positions[num][1];
	}
	
	public void setPosOf(int num, int row, int col) {
		positions[num][0] = row;
		positions[num][1] = col;
	}
	
	public int numBlocks() {
		return positions.length;
	}
	
	public Color getColor() {
		return color;
	}
	
	public boolean getIsFixed() {
		return isFixed;
	}
	
	public void fix() {
		isFixed = true;
	}
	
	public boolean isPartOfSelf(int r, int c) {
		for(int row = 0; row < positions.length; row++) {
			if(positions[row][0] == r && positions[row][1]==c) {
				return true;
			}
		}
		return false;
	}
	
	public void rotate() {
		for(int block = 1; block < numBlocks(); block++) {
			if(getRowOf(block) == getRowOf(0)-1 && getColOf(block) == getColOf(0)-1) {
				setPosOf(block, getRowOf(0)-1, getColOf(0)+1);
			} else if(getRowOf(block) == getRowOf(0)-1 && getColOf(block) == getColOf(0)) {
				setPosOf(block, getRowOf(0), getColOf(0)+1);
			} else if(getRowOf(block) == getRowOf(0)-1 && getColOf(block) == getColOf(0)+1) {
				setPosOf(block, getRowOf(0)+1, getColOf(0)+1);
			} else if(getRowOf(block) == getRowOf(0) && getColOf(block) == getColOf(0)-1) {
				setPosOf(block, getRowOf(0)-1, getColOf(0));
			} else if(getRowOf(block) == getRowOf(0) && getColOf(block) == getColOf(0)+1) {
				setPosOf(block, getRowOf(0)+1, getColOf(0));	
			} else if(getRowOf(block) == getRowOf(0)+1 && getColOf(block) == getColOf(0)-1) {
				setPosOf(block, getRowOf(0)-1, getColOf(0)-1);
			} else if(getRowOf(block) == getRowOf(0)+1 && getColOf(block) == getColOf(0)) {
				setPosOf(block, getRowOf(0), getColOf(0)-1);
			} else if(getRowOf(block) == getRowOf(0)+1 && getColOf(block) == getColOf(0)+1) {
				setPosOf(block, getRowOf(0)+1, getColOf(0)-1);
			} 
			
		}

	}
	
	public void wallKick() {
		for(int i = 1; i < numBlocks(); i++) {
			while(getRowOf(i) < 0) {
				setPosOf(0, getRowOf(0) + 1, getColOf(0));
				setPosOf(1, getRowOf(1) + 1, getColOf(1));
				setPosOf(2, getRowOf(2) + 1, getColOf(2));
				setPosOf(3, getRowOf(3) + 1, getColOf(3));
			}
			
			while(getRowOf(i) > getNumRows()-1) {
				setPosOf(0, getRowOf(0)-1, getColOf(0));
				setPosOf(1, getRowOf(1)-1, getColOf(1));
				setPosOf(2, getRowOf(2)-1, getColOf(2));
				setPosOf(3, getRowOf(3)-1, getColOf(3));
			}
			
			while(getColOf(i) > getNumCols()-1) {
				setPosOf(0, getRowOf(0), getColOf(0)-1);
				setPosOf(1, getRowOf(1), getColOf(1)-1);
				setPosOf(2, getRowOf(2), getColOf(2)-1);
				setPosOf(3, getRowOf(3), getColOf(3)-1);
			}
			
			while(getColOf(i) < 0) {
				setPosOf(0, getRowOf(0), getColOf(0)+1);
				setPosOf(1, getRowOf(1), getColOf(1)+1);
				setPosOf(2, getRowOf(2), getColOf(2)+1);
				setPosOf(3, getRowOf(3), getColOf(3)+1);
			}
			
			
		}
	}
	
}
