import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

public class Model {
	private Color[][] grid;
	private static final String GRID_NULL_MSG = "grid cannot be null.";
	private ArrayList<GridListener> gridListeners;
	private ArrayList<LevelListener> levelListeners;
	int level, clearedLines;
	Shape last;
	
	public Model() {
		clearModel();
		gridListeners = new ArrayList<>();
		levelListeners = new ArrayList<>();
		level = 0;
		clearedLines = 0;
	}
	
	public void clearModel() {
		grid = new Color[15][10];
		for(int row  = 0; row< grid.length; row++) {
			for(int col  = 0; col < grid[0].length; col++) {
				grid[row][col] = Color.WHITE;
				setColorAt(row,col, Color.WHITE);
			}
		}
		last = null;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int l) {
		level = l;
		for(int i = 0; i < levelListeners.size(); i++) {
			levelListeners.get(i).levelChanged(l-1, l);
		 }
	}
	
	public int getNumLinesCleared() {
		return clearedLines;
	}
	
	public void setNumLinesCleared(int l) {
		clearedLines = l;
		for(int i = 0; i < levelListeners.size(); i++) {
			levelListeners.get(i).numLinesCleared(l);
		 }
		if(getNumLinesCleared() >= 5) {
			setLevel(getLevel()+1);
		}
	}
	
	public void addGridListener(GridListener l) {
		if (!gridListeners.contains(l)) {
			gridListeners.add(l);
		}
	}
	
	public void removeGridListener(GridListener l) {
		gridListeners.remove(l);
	}
	
	public void addLevelListener(LevelListener l) {
		if (!levelListeners.contains(l)) {
			levelListeners.add(l);
		}
	}
	
	public void removeLevelListener(LevelListener l) {
		levelListeners.remove(l);
	}
	
	public int getNumRows() {
		return grid.length;
	}
	
	public int getNumCols() {
		if(grid.length > 0) {
			return grid[0].length;
		}
		return -1;
	}
	
	public void rotate() {
		if(last != null && !last.getIsFixed()) {
			for(int block = 0; block < last.numBlocks(); block++) {
				setColorAt(last.getRowOf(block), last.getColOf(block), Color.WHITE);
			}
			last.rotate();
			last.wallKick();
			boolean positioned = false;			
			while(!positioned) {
				for(int block = 0; block < last.numBlocks(); block++) {
					if(last.getRowOf(block) < 0 || last.getRowOf(block) > getNumRows()-2 ||
					(getColorAt(last.getRowOf(block),last.getColOf(block))!= Color.WHITE &&
					!last.isPartOfSelf(last.getRowOf(block),last.getColOf(block)))) {
						for(int b = 0; b < last.numBlocks(); b++) {
							last.setPosOf(b, last.getRowOf(b)-1, last.getColOf(b));
						}
						positioned = false;
						return;
					}
				}
				positioned = true;
				
			}

			for(int block = 0; block < last.numBlocks(); block++) {
				setColorAt(last.getRowOf(block), last.getColOf(block), last.getColor());
			}
		}
		
		for(int block = 0; block < last.numBlocks(); block++) {
			if(last.getRowOf(block) > getNumRows()-2 ||
					(getColorAt(last.getRowOf(block)+1,last.getColOf(block))!= Color.WHITE &&
					!last.isPartOfSelf(last.getRowOf(block)+1,last.getColOf(block)))) {
				last.fix();
			}
		}
	}
	
	public void move(KeyCode kc) {
		int changeCol = 0, changeRow = 0;
		if(kc.equals(KeyCode.DOWN)) {
			changeRow = 1;
			changeCol = 0;
		} else if (kc.equals(KeyCode.LEFT)) {
			changeRow = 0;
			changeCol = -1;
		} else if (kc.equals(KeyCode.RIGHT)){
			changeRow = 0;
			changeCol = 1;
		}
		
		
		if(last != null && !last.getIsFixed()) {
			for(int block = 0; block < last.numBlocks(); block++) {
				if(last.getRowOf(block)+changeRow < 0 ||  last.getRowOf(block)+changeRow > getNumRows() -1 || 
				   last.getColOf(block)+changeCol < 0 ||  last.getColOf(block)+changeCol > getNumCols() -1||
						   (getColorAt(last.getRowOf(block)+changeRow,last.getColOf(block)+changeCol)!= Color.WHITE &&
							!last.isPartOfSelf(last.getRowOf(block)+changeRow,last.getColOf(block)+changeCol))) {
					return;
				}
			}
			
			for(int block = 0; block < last.numBlocks(); block++) {
				setColorAt(last.getRowOf(block), last.getColOf(block), Color.WHITE);
			}
			
			for(int block = 0; block < last.numBlocks(); block++) {
				last.setPosOf(block, last.getRowOf(block)+changeRow, last.getColOf(block)+changeCol);
				setColorAt(last.getRowOf(block), last.getColOf(block), last.getColor());
				if(last.getRowOf(block) > getNumRows()-2 ||
						(getColorAt(last.getRowOf(block)+1,last.getColOf(block))!= Color.WHITE &&
						!last.isPartOfSelf(last.getRowOf(block)+changeRow,last.getColOf(block)))) {
					last.fix();
				}
			}
			if(last.getIsFixed()) {
				for(int block = 0; block < last.numBlocks(); block++) {
					if(last.getRowOf(block) == 0) {
						System.out.println("over");
						for(int i = 0; i < levelListeners.size(); i++) {
							levelListeners.get(i).gameOver();
						 }
					}
				}
			}
		}
	}
	
	public Color getColorAt(int row, int col) {
		return this.grid[row][col];
	}
	
	public void setColorAt(int row, int col, Color color) {
		Color oldVal = this.grid[row][col];
		this.grid[row][col] = color;
		if (oldVal == null && color != null || oldVal != null && !oldVal.equals(color)) {
			for (GridListener l : gridListeners) {
				l.cellChanged(row, col, color);
			}
		}
		
	}
	
	public boolean isFullRow(int row) {
		int num = 0;
		for (int i = 0; i < getNumCols(); i++) {
			if(getColorAt(row,i) != Color.WHITE) {
				num++;
			}
		}
		return num == getNumCols();
	}
	
	public void cleanFullRows() {
		for(int row = getNumRows()-1; row > 0; row--) {
			if(isFullRow(row)) {
				for(int col  = 0; col < getNumCols(); col++) {
					setColorAt(row, col, Color.WHITE);
				}
				
				for(int r = row-1; r > 1; r--) {
					for(int col  = 0; col < getNumCols(); col++) {
						if(!last.isPartOfSelf(r, col) || 
								(last.isPartOfSelf(r, col) && last.getIsFixed())) {
							setColorAt(r+1, col, getColorAt(r,col));
							setColorAt(r, col, Color.WHITE);
						}
					}
				}
				setNumLinesCleared(getNumLinesCleared()+1);
			}
		}
	}
	
	public void update() {
		if(last != null && !last.getIsFixed()) {
			move(KeyCode.DOWN);
			cleanFullRows();
		} else {
			Shape changer;
			int rand;
			rand = (int) (Math.random()*7);
//			rand = (int) (Math.random()*2);
//			rand = 0;
			if(rand == 0) {
				changer = new IShape(getNumRows(),getNumCols());
			} else if(rand == 1) {
				changer = new Oshape(getNumRows(),getNumCols());
			} else if(rand == 2) {
				changer = new Tshape(getNumRows(),getNumCols());
			} else if(rand == 3) {
				changer = new Zshape(getNumRows(),getNumCols());
			} else if(rand == 4) {
				changer = new Lshape(getNumRows(),getNumCols());
			} else if(rand == 5) {
				changer = new Sshape(getNumRows(),getNumCols());
			} else  {
				changer = new Jshape(getNumRows(),getNumCols());
			}
			last = changer;
			for(int i = 0; i < changer.numBlocks(); i++) {
				if(getColorAt(changer.getRowOf(i),changer.getColOf(i)) != Color.WHITE) {
					for(int x = 0; x < levelListeners.size(); x++) {
						levelListeners.get(x).gameOver();
					 }
				}
				setColorAt(changer.getRowOf(i),changer.getColOf(i), changer.getColor());
				
				
			}
			
		}
		cleanFullRows();
	}
}
