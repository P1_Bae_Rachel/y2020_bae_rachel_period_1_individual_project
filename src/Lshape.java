import javafx.scene.paint.Color;

public class Lshape extends Shape {
	public Lshape(int rows, int cols) {
		super(rows, cols,Color.LIGHTBLUE);
		positions = new int[4][2];
		positions[0][0] = 1;
		positions[0][1] = getNumCols()/2;
		positions[1][0] = 0;
		positions[1][1] = getNumCols()/2;
		positions[2][0] = 2;
		positions[2][1] = getNumCols()/2;
		positions[3][0] = 2;
		positions[3][1] = getNumCols()/2+1;
	}
}
