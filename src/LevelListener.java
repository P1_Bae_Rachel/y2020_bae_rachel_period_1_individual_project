
public interface LevelListener {
	public void numLinesCleared(int numCleared);
	public void levelChanged(int oldVal, int newVal);
	public void gameOver();
}
