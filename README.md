# README #

**Name:**	Rachel Bae

**Period:**	1

**Game Title:** Tetris

## Game Proposal ##

I want to make a clone of Tetris. I will try to show scoring, level, number of lines cleared, and next 2 to 3 blocks,
but I am not sure if I will be able to show the shadow of the block when it is placed down as the original game.
For each level, along with the speed of falling blocks and number of lines required to clear, I will try to add my own shapes
that are not included in the original Tetris game. If possible, I will try to make two blocks fall at once, where one will be
controlled by the arrows and one will be controlled with the AWDS keys. If that does not work, I would make some elements such as
"stone" blocks which will not be able to rotate around. I could also add "power ups" where a certain amount of points could be
exchanged with the power to slow down the speed, select the next element the user wants, etc.

Game Controls:

+ Down, left, and right button for direction of each block
+ Up button for rotating the block around.
+ In the original game, if you click where you want to place the block, the block lands, but I am not sure if I will be able to execute this.

Game Elements:

+ Unlimited number of levels
+ New shape(s) added each level
+ One or two blocks dropping at a time depending on level
+ Speed increases each level
+ "Stone blocks" which the player will not be able to rotate around.
+ Power ups such as controlling time, selecting the next element, deleting all blocks of a certian color, etc.

How to Win:

+ Unlimited number of levels
+ Could win a level by clearing the required number of lines or achieving a certian score required for each level.

## Link Examples ##
Provide links to examples of your game idea.  This can be a playable online game, screenshots, YouTube videos of gameplay, etc.

+ [Tetris](https://tetris.com/play-tetris) My original inspiration

## Teacher Response ##

**Approved**

I've written Tetris before and it's not that easy.  You won't need powerups or special features since Tetris is enough by itself as long
as you make a smooth-feeling game and not an awkwardly controlled one.  One suggestion that will make things easier is to only move blocks
by whole grid-squares at a time.  In other words, don't go for smoothly falling pieces.  Let pieces drop down one line at a time, but at
different speeds as levels increase.  Your main task will be in how to represent the shapes internally and how to fit them together.
Most of your game can be written on the back end (Model) and the View just displays whatever the model has.  Finally, start with only
straight lines (that can rotate) and 4-blocks.  Then move onto shapes that have deadspace like T-tile or L-tile.

## Class Design and Brainstorm ##

Put all your brainstorm ideas, strategy approaches, and class outlines here

+ Just simple tetris with no power ups
+ similar grid concept of grid as LifeGUI
+ Borrowing grid and generationlistener concept from LifeGUI
+ Determine level based on number of lines cleared

## Development Journal ##

Every day you work, keep track of it here.

**Date (time spent)**

Goal:  What are you trying to accomplish today?

Work accomplished:  Describe what you did today and how it went.

**Monday May 18 (1 hour)**

Goal:  Making a demo of Tetris by making a single cube fall down a grid at a time.

Work accomplished:  I mainly thought that the movement of the cubes is similar to the LifeGUI game. Because the adjustments did not go smoothly, just coding the demo took quite a long time, and I was not able to finish coding. 
However, from the coding so far, the idea seems logical.

**Tuesday May 19 (1 hour)**

Goal:  Making shapes that fall down and move together

Work accomplished: I was able to finish the falling single blocks in 20 minutes of today, but I was not able to finish removing an entire line of blocks when filled up. I am constantly getting bound errors and null exception errors. I am not sure about the null exception errors.

**Wednesday May 20 (30 Minutes)**

Goal:  Finishing the demo of Tetris by making one full line disappear when it is full.

Work accomplished: I was able to finish full (4) blocks falling in 20 minutes of today, but I was not able to finish removing an entire line of blocks when filled up. I am constantly getting bound errors and null exception errors. I am not sure about the null exception errors.

**Thursday May 21 (1 Hour)**

Goal:  Making a full line disappear and filling it with the above lines.

Work accomplished:  I constantly got bound errors and issues with my for loops, but I still was able to clear full lines and pulling the rest of the blocks that are above it. It does not look very smooth, but I am not sure how to edit that issue. Currently, I am not sure whether my methods do not have any errors regarding the clearing

**Friday May 22 (30 Minutes)**

Goal:  Create multiple shapes, control and keep track of all the blocks on the field

Work accomplished: I easily created multiple shapes, but I did not have much time to work on keeping all the information of the blocks on the Tetris board.

**Saturday May 23 (2 hour)**

Goal:  Rid of templates that the teachers have given from LifeGUI and make my own model.

Work accomplished: For the past week, as part of the demo to test whether my idea would work, I used some of the code that the teachers have given us for the LifeGUI assignment because it was really helpful to see the Model, View, and Controller work together easily. However, a lot of the methods were unnecessary for me and the design is not exactly fit for me, so I tried to organize my own Controller. I still liked the View, so I kept it and I am not sure what I can change from it. I am not entirely down with redesigning the Model yet. Nothing comes up on the java window anymore. 

**Monday May 25 (2 hour)**

Goal:  Finish recoding Model and Move the blocks with keys

Work accomplished: The Java window presents the grid and the blocks, but I still had a lot of errors regarding the shapes. I deleted the Generation Listener, but I am not sure whether I would use it later for changing levels. It could be handy when I would like to change levels. Then, I tried using key input to move the blocks. I did not request focus in the beginning for about 20 minutes, so the keys were not working. I also had quite a few issues with bounds.

**Tuesday May 26 (2.5 Hours)**

Goal:  Finish moving blocks, Figure how to rotate the blocks

Work accomplished: I have finished all the code for moving the blocks down, left and right. For rotating blocks, I first considered coding for the 4 rotations for each block separately. It took a long time, but after I researched a bit about the rotation methods, I figured to use the "Super rotation system" that is described as the method the actual Tetris game goes by. However, I am still having some problems regarding bounds after the shapes are rotated. Also, sometimes, when I clear some rows, some of the blocks of the moving piece changes, and I am not sure how to fix it.

**Wednesday May 27 (2 Hours)**

Goal:  Finish positioning blocks after rotation, edit how to save/track the colors in blocks when clearing rows

Work accomplished: I finished positioning of blocks after rotations, and I edited how to track colors when clearing rows. However, I noticed that sometimes, even when the block realizes that it cannot go down anymore, it does not realize that it is fixed, so the next block does not come down. I am not sure where the issue is.

**Thursday May 28 (2 Hours)**

Goal:  Add transitions from different scenes (title, game over, and possibly pause the game in the middle)

Work accomplished: I successfully added a title scene, a game over scene, and I am able to pause the game in the middle. Sometimes, changing from different scenes did not work smoothly. The view/model would not clear up, level would not change as I wanted to, but from the numerous times I tested out, I think the change in scenes work properly now. However, I am worried that I have unnecessary code for changing between scenes, and I am not sure how to identify which parts are unnecessary.

***